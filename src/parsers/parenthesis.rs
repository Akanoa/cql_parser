use nom::IResult;
use nom::bytes::complete::{tag, is_not};
use nom::combinator::{recognize, map};
use nom::sequence::{tuple};
use nom::multi::many0;
use nom::branch::alt;

pub fn inner_parenthesised_group(i: &str) -> IResult<&str, &str> {
    map(parse_parenthesised_group, |s| &s[ 1 .. s.len() - 1])(i)
}

fn parse_parenthesised_group(i: &str) -> IResult<&str, &str> {
    let parse = recognize(tuple((
        tag("("),
        many0(alt((is_not("()"), parse_parenthesised_group))),
        tag(")"),
    )));
    parse(i)
}

#[cfg(test)]
mod tests {
    use crate::parsers::parenthesis::{parse_parenthesised_group, inner_parenthesised_group};

    #[test]
    fn test_parse_parenthesised_group() {
        assert_eq!(parse_parenthesised_group("()"), Ok(("", "()")));
        assert_eq!(parse_parenthesised_group("( )"), Ok(("", "( )")));
        assert_eq!(parse_parenthesised_group("( 12 )"), Ok(("", "( 12 )")));
        assert_eq!(parse_parenthesised_group("( 12, 42 )"), Ok(("", "( 12, 42 )")));
        assert_eq!(parse_parenthesised_group("( 12 42 )"), Ok(("", "( 12 42 )")));
        assert_eq!(parse_parenthesised_group("( any character except ) )"), Ok((" )", "( any character except )")));
        assert_eq!(parse_parenthesised_group("( 12, toto, \"escaped data$$ ^^\" )"), Ok(("", "( 12, toto, \"escaped data$$ ^^\" )")));
    }

    #[test]
    fn test_inner_parenthesised_group() {
        assert_eq!(inner_parenthesised_group("()"), Ok(("", "")));
        assert_eq!(inner_parenthesised_group("( )"), Ok(("", " ")));
        assert_eq!(inner_parenthesised_group("(test)"), Ok(("", "test")));
        assert_eq!(inner_parenthesised_group("(test (truc))"), Ok(("", "test (truc)")));
        assert_eq!(inner_parenthesised_group("(test (truc))  machin"), Ok(("  machin", "test (truc)")));
    }
}