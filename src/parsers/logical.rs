use nom::character::complete::{multispace0};
use nom::IResult;
use nom::sequence::delimited;
use nom::combinator::{map, complete};
use nom::bytes::complete::tag;
use nom::branch::alt;


#[derive(Debug, Eq, PartialEq, Clone)]
pub enum LogicalOperator {
    And,
    Or
}

pub fn parse_logical_operator(i: &str) -> IResult<&str, LogicalOperator> {
    map(delimited(
        multispace0,
        alt((
            complete(tag("OR")),
            complete(tag("AND"))
            )),
        multispace0
    ), |val: &str| {
        match val {
            "AND" => LogicalOperator::And,
            _ => LogicalOperator::Or
        }
    })(i)
}

#[cfg(test)]
mod tests {
    use crate::parsers::logical::{LogicalOperator, parse_logical_operator};
    use nom::Err::Error;
    use nom::error::ErrorKind::{Tag};

    #[test]
    fn test_parse_logical_operator() {
        assert_eq!(parse_logical_operator(" OR"), Ok(("", LogicalOperator::Or)));
        assert_eq!(parse_logical_operator(" AND "), Ok(("", LogicalOperator::And)));
        assert_eq!(parse_logical_operator("SOMETHING "), Err(Error(("SOMETHING ", Tag))));
    }
}