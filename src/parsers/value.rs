use nom::character::complete::{alphanumeric1, digit1, multispace0, space0};
use crate::parsers::misc::{parse_date, parse_float};
use nom::IResult;
use nom::branch::alt;
use nom::combinator::{complete, map, opt};
use nom::sequence::{delimited, tuple};
use nom::bytes::complete::{tag, take_until};
use nom::multi::many0;

pub fn parse_value(i: &str) -> IResult<&str, &str> {
    alt((
        complete(parse_date),
        complete(parse_float),
        complete(alphanumeric1),
        complete(digit1),
        delimited(
            tag("\""),
            take_until("\""),
            tag("\"")
        )
        ))(i)
}

fn parse_value_comma_separated(i: &str) -> IResult<&str, String> {
    map(
        tuple((
            space0,
            parse_value,
            opt( complete(tag(",") )),
            space0
        )), |(_, data, _, _)| data.to_string()
    )(i)
}

fn parse_in_group(i: &str) -> IResult<&str, Vec<String>> {
    map(delimited(
        multispace0,
        tuple((
            tag("("),
            space0,
            many0(parse_value_comma_separated),
            space0,
            tag(")")
            )),
        multispace0
    ), |(_, _, data, _, _)| data)(i)
}

#[cfg(test)]
mod tests {

    use crate::parsers::value::{parse_value, parse_in_group, parse_value_comma_separated};

    #[test]
    fn test_parse_value() {
        assert_eq!(parse_value("\"je suis une variable\""), Ok(("", "je suis une variable")));
        assert_eq!(parse_value("12"), Ok(("", "12")));
        assert_eq!(parse_value("12.5"), Ok(("", "12.5")));
        assert_eq!(parse_value("12,5"), Ok(("", "12,5")));
        assert_eq!(parse_value("12/07/2015toto"), Ok(("toto", "12/07/2015")));
        assert_eq!(parse_value("\"12/07/2015toto\""), Ok(("", "12/07/2015toto")));
    }

    #[test]
    fn test_parse_value_comma_separated() {
        assert_eq!(parse_value_comma_separated("12"), Ok(("", "12".to_string())));
        assert_eq!(parse_value_comma_separated("12,"), Ok(("", "12".to_string())));
        assert_eq!(parse_value_comma_separated("toto,"), Ok(("", "toto".to_string())));
        assert_eq!(parse_value_comma_separated("\"Je suis une variable\","), Ok(("", "Je suis une variable".to_string())));
    }

    #[test]
    fn test_parse_in_group() {
        assert_eq!(parse_in_group("()"), Ok(("", vec![])));
        assert_eq!(parse_in_group("( 12 )"), Ok(("", vec!["12".to_string()])));
        assert_eq!(parse_in_group("( 12, 42 )"), Ok(("", vec!["12".to_string(), "42".to_string()])));
        assert_eq!(parse_in_group("( 12, toto, \"escaped data$$ ^^\" )"), Ok(("", vec!["12".to_string(), "toto".to_string(), "escaped data$$ ^^".to_string()])));
    }
}