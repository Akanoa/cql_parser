use nom::character::complete::{alphanumeric1, multispace0};
use crate::parsers::value::parse_value;
use nom::IResult;
use nom::multi::fold_many0;
use nom::combinator::{complete, map};
use nom::bytes::complete::tag;
use nom::branch::alt;
use nom::sequence::{tuple, delimited};
use crate::parsers::expression::FilterGroupComponent::LogicalOperator;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum FilterSecondMember {
    Operator {operator: Operator, value: String},
    Group(Vec<String>)
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Filter {
    pub property: String,
    pub second_member: FilterSecondMember,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Operator {
    Equal,
    Different,
    LessThan,
    GreaterThan,
    GreaterOrEqualThan,
    LessOrEqualThan,
    Interval
}

//
// -- parsers macros
//

fn parse_variable_name(i: &str) -> IResult<&str, String> {
    fold_many0(
        alt((
            complete(tag("_")),
            complete(alphanumeric1)
        )), "".to_string(), |mut acc, item: &str| {
            acc = format!("{}{}", acc, item.to_string());
            acc
        }
    )(i)
}

fn parse_property(i: &str) -> IResult<&str, String> {
    map(tuple((
        tag("@"),
        parse_variable_name
    )), |(_, name)|name)(i)
}

// <=>|<=|< | >= | > | =
fn parse_operator(i: &str) -> IResult<&str, Operator> {
    delimited(
        multispace0,
        alt((
            map(complete(tag("<=>")), |_| Operator::Interval),
            map(complete(tag("!=")), |_| Operator::Different),
            map(complete(tag("<=")), |_| Operator::LessOrEqualThan),
            map(complete(tag("<")), |_| Operator::LessThan),
            map(complete(tag(">=")), |_| Operator::GreaterOrEqualThan),
            map(complete(tag(">")), |_| Operator::GreaterThan),
            map(complete(tag("=")), |_| Operator::Equal)
        )),
        multispace0
    )(i)
}

pub fn parse_filter(i: &str) -> IResult<&str, Filter> {
    map(tuple((
        parse_property,
        parse_operator,
        parse_value
        )), |(property, operator, value) : (String, Operator, &str)| {
        Filter {
            property,
            second_member: FilterSecondMember::Operator {operator, value:value.to_string() }
        }
    })(i)
}

#[cfg(test)]
mod tests {

    use crate::parsers::filter::{parse_variable_name, parse_property, parse_operator, parse_filter, Operator, Filter, FilterSecondMember};
    use nom::error::ErrorKind::{Tag};
    use nom::Err::Error;

    #[test]
    fn test_parse_variable_name() {
        assert_eq!(parse_variable_name("test"), Ok(("", "test".to_string())));
        assert_eq!(parse_variable_name("test_"), Ok(("", "test_".to_string())));
        assert_eq!(parse_variable_name("test2 "), Ok((" ", "test2".to_string())));
    }

    #[test]
    fn test_parse_property() {
        assert_eq!(parse_property("@test"), Ok(("", "test".to_string())));
        assert_eq!(parse_property("@my_property"), Ok(("", "my_property".to_string())));
        assert_eq!(parse_property("@my_property** #"), Ok(("** #", "my_property".to_string())));
        assert_eq!(parse_property("@my_property2"), Ok(("", "my_property2".to_string())));
    }

    #[test]
    fn test_parse_operator() {
        assert_eq!(parse_operator(" = "), Ok(("", Operator::Equal)));
        assert_eq!(parse_operator("="), Ok(("", Operator::Equal)));
        assert_eq!(parse_operator(">"), Ok(("", Operator::GreaterThan)), ">");
        assert_eq!(parse_operator("<"), Ok(("", Operator::LessThan)));
        assert_eq!(parse_operator("<="), Ok(("", Operator::LessOrEqualThan)));
        assert_eq!(parse_operator(">="), Ok(("", Operator::GreaterOrEqualThan)));
        assert_eq!(parse_operator("!="), Ok(("", Operator::Different)));
        assert_eq!(parse_operator("<=>"), Ok(("", Operator::Interval)));
        assert_eq!(parse_operator("??"), Err(Error(("??", Tag))));
        assert_eq!(parse_operator("= 12"), Ok(("12", Operator::Equal)));
    }

    #[test]
    fn test_parse_filter() {
        assert_eq!(parse_filter("@my_property = 12toto"), Ok(("toto", Filter {
            property: "my_property".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "12".to_string()
            }
        })));
        assert_eq!(parse_filter("@test2 < 4"), Ok(("", Filter {
            property: "test2".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::LessThan,
                value: "4".to_string()
            }
        })));
    }
}