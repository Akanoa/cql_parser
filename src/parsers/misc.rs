use nom::character::complete::{digit1};
use nom::number::complete::recognize_float;
use nom::IResult;
use nom::combinator::{recognize, map_parser, complete};
use nom::sequence::tuple;
use nom::bytes::complete::{take, tag};
use nom::branch::alt;

fn parse_french_date(i: &str) -> IResult<&str, &str> {
    recognize(tuple((
        map_parser(take(2 as usize), digit1),
        tag("/"),
        map_parser(take(2 as usize), digit1),
        tag("/"),
        map_parser(take(4 as usize), digit1),
        )))(i)
}

pub fn parse_date(i: &str) -> IResult<&str, &str> {
    parse_french_date(i)
}

pub fn parse_float(i: &str) -> IResult<&str, &str> {
    alt((
        complete(parse_float_comma_separated),
        complete(recognize_float)
        ))(i)
}

pub fn parse_float_comma_separated(i: &str) -> IResult<&str, &str> {
    recognize(tuple((
        digit1,
        tag(","),
        digit1
        )))(i)
}

#[cfg(test)]
mod tests {

    use crate::parsers::misc::{parse_float_comma_separated, parse_float, parse_french_date, parse_date};

    #[test]
    fn test_parse_float_comma_separated() {
        assert_eq!(parse_float_comma_separated("12,55"), Ok(("", "12,55")));
    }

    #[test]
    fn test_parse_float() {
        assert_eq!(parse_float("12,55"), Ok(("", "12,55")));
        assert_eq!(parse_float("12.55"), Ok(("", "12.55")));
    }

    #[test]
    fn test_parse_french_date() {
        assert_eq!(parse_french_date("20/07/1992"), Ok(("", "20/07/1992")));
    }

    #[test]
    fn test_parse_date() {
        assert_eq!(parse_date("20/07/1992"), Ok(("", "20/07/1992")));
    }
}