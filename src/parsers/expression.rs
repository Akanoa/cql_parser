use crate::parsers::filter::{Filter, parse_filter};
use crate::parsers::logical::{LogicalOperator, parse_logical_operator};
use crate::parsers::parenthesis;
use nom::character::complete::{space0, multispace0};
use nom::IResult;
use nom::multi::many0;
use nom::combinator::{map, flat_map, opt, complete, map_res, map_parser};
use nom::sequence::{tuple, delimited};
use nom::branch::alt;
use nom::error::ParseError;


#[derive(Debug, Eq, PartialEq, Clone)]
pub enum FilterGroupComponent {
    Filter(Filter),
    LogicalOperator(LogicalOperator)
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum ExpressionGroup {
    FilterGroup(Vec<FilterGroupComponent>),
    ParenthesisedGroup(Vec<FilterGroupComponent>)
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum ExpressionGroupComponent {
    ExpressionGroup(ExpressionGroup),
    LogicalOperator(LogicalOperator)
}

fn parse_multi_filter_component(i: &str) -> IResult<&str, Vec<(LogicalOperator, Filter)>> {
    many0(
        map(tuple((
            space0,
            parse_logical_operator,
            space0,
            parse_filter
            )), |(_, op, _, filter)| (op, filter))
    )(i)
}

fn parse_filter_group(i: &str) -> IResult<&str, Vec<FilterGroupComponent>> {
    map(
        delimited(
            multispace0,
            tuple((
                parse_filter,
                parse_multi_filter_component
                )),
            multispace0
        ), |t| map_filter_group_component(t)
    )(i)
}

fn parse_expression_group(i: &str) -> IResult<&str, ExpressionGroup> {
    alt((
        map(parse_filter_group, |group| ExpressionGroup::FilterGroup(group)),
        map(map_parser(parenthesis::inner_parenthesised_group, parse_filter_group), |group| ExpressionGroup::ParenthesisedGroup(group))
    ))(i)
}

fn parse_multi_expression_group_component(i: &str) -> IResult<&str, Vec<(LogicalOperator, ExpressionGroup)>> {
    many0(
       map(
           tuple((
                space0,
               parse_logical_operator,
               space0, parse_expression_group
               )), |(_, op, _, expression)| (op, expression)
       )
    )(i)
}

pub fn parse_expression(i: &str) -> IResult<&str, Vec<ExpressionGroupComponent>> {
    map(
        delimited(
            multispace0,
            tuple((
                parse_expression_group,
                parse_multi_expression_group_component
                )),
            multispace0
        ), |t| map_filter_expression_group_component(t)
    )(i)
}


//
// -- parsers
//

fn map_filter_group_component((f, v): (Filter, Vec<(LogicalOperator, Filter)>)) -> Vec<FilterGroupComponent> {

    let fold_results = vec![FilterGroupComponent::Filter(f)];

    v.into_iter().fold(fold_results, |mut acc, (l, f)| {
        acc.push(FilterGroupComponent::LogicalOperator(l));
        acc.push(FilterGroupComponent::Filter(f));
        acc
    })

}

fn map_filter_expression_group_component((e, v): (ExpressionGroup, Vec<(LogicalOperator, ExpressionGroup)>)) -> Vec<ExpressionGroupComponent> {

    let fold_results = vec![ExpressionGroupComponent::ExpressionGroup(e)];

    v.into_iter().fold(fold_results, |mut acc, (l, e)| {
        acc.push(ExpressionGroupComponent::LogicalOperator(l));
        acc.push(ExpressionGroupComponent::ExpressionGroup(e));
        acc
    })

}

#[cfg(test)]
mod tests {
    use crate::parsers::filter::{Filter, Operator, FilterSecondMember};
    use crate::parsers::expression::{parse_multi_filter_component, map_filter_group_component, FilterGroupComponent, ExpressionGroup, ExpressionGroupComponent, map_filter_expression_group_component, parse_expression_group, parse_multi_expression_group_component, parse_filter_group, parse_expression};
    use crate::parsers::logical::LogicalOperator;
    use nom::Err::Error;
    use nom::error::ErrorKind::{Tag};

    #[test]
    fn test_parse_multi_filter_component() {
        assert_eq!(parse_multi_filter_component(" AND @test = 12"), Ok(("",vec![(LogicalOperator::And, Filter {
            property: "test".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "12".to_string()
            }
        })])));
        assert_eq!(parse_multi_filter_component(" AND @test2 < 4"), Ok(("",vec![(LogicalOperator::And, Filter {
            property: "test2".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::LessThan,
                value: "4".to_string()
            }
        })])));
    }

    #[test]
    fn test_parse_filter_group() {
        let f = Filter {
            property: "test".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "12".to_string()
            }
        };
        let f2 = Filter {
            property: "test2".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::LessThan,
                value: "4".to_string()
            }
        };
        let f3 = Filter {
            property: "paid".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "true".to_string()
            }
        };
        assert_eq!(parse_filter_group("@test = 12"), Ok(("",
            vec![
             FilterGroupComponent::Filter(f.clone()),
            ]
        )));
        assert_eq!(parse_filter_group(" @test = 12 "), Ok(("",
            vec![
             FilterGroupComponent::Filter(f.clone()),
            ]
        )));
        assert_eq!(parse_filter_group(" @test = 12 AND @test2 < 4 "), Ok(("",
            vec![
              FilterGroupComponent::Filter(f.clone()),
              FilterGroupComponent::LogicalOperator(LogicalOperator::And),
              FilterGroupComponent::Filter(f2.clone())
            ]
        )));
        assert_eq!(parse_filter_group("@test = 12 AND @test2 < 4 OR @paid = true"), Ok(("",
            vec![
                FilterGroupComponent::Filter(f.clone()),
                FilterGroupComponent::LogicalOperator(LogicalOperator::And),
                FilterGroupComponent::Filter(f2.clone()),
                FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
                FilterGroupComponent::Filter(f3.clone())
            ]
        )));
    }

    #[test]
    fn test_map_filter_group_component(){
        let f = Filter {
            property: "test".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "12".to_string()
            }
        };
        let f2 = Filter {
            property: "test2".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::LessThan,
                value: "4".to_string()
            }
        };
        let f3 = Filter {
            property: "paid".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "true".to_string()
            }
        };
        assert_eq!(map_filter_group_component((f.clone(), vec![])),
                   vec![FilterGroupComponent::Filter(f.clone())]);
        assert_eq!(map_filter_group_component((f.clone(), vec![(LogicalOperator::And, f2.clone())])),
                   vec![
                       FilterGroupComponent::Filter(f.clone()),
                       FilterGroupComponent::LogicalOperator(LogicalOperator::And),
                       FilterGroupComponent::Filter(f2.clone()),
                   ]
        );
        assert_eq!(map_filter_group_component((f.clone(), vec![(LogicalOperator::And, f2.clone()),(LogicalOperator::Or, f3.clone())])),
                   vec![
                       FilterGroupComponent::Filter(f.clone()),
                       FilterGroupComponent::LogicalOperator(LogicalOperator::And),
                       FilterGroupComponent::Filter(f2.clone()),
                       FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
                       FilterGroupComponent::Filter(f3.clone()),
                   ]
        );
    }

    #[test]
    fn test_parse_expression_group() {
        let f = Filter {
            property: "test".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "12".to_string()
            }
        };
        let f2 = Filter {
            property: "test2".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::LessThan,
                value: "4".to_string()
            }
        };
        let f3 = Filter {
            property: "paid".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "true".to_string()
            }
        };

        let group1 = vec![
            FilterGroupComponent::Filter(f.clone()),
            FilterGroupComponent::LogicalOperator(LogicalOperator::And),
            FilterGroupComponent::Filter(f2.clone()),
            FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
            FilterGroupComponent::Filter(f3.clone()),
        ];

        assert_eq!(parse_expression_group("@test = 12 AND @test2 < 4 OR @paid = true"),
                   Ok(("", ExpressionGroup::FilterGroup(group1.clone())))
        );
        assert_eq!(parse_expression_group("( @test = 12 AND @test2 < 4 OR @paid = true )"),
                   Ok(("", ExpressionGroup::ParenthesisedGroup(group1.clone())))
        );
    }

    #[test]
    fn test_map_expression_component() {
        let f = Filter {
            property: "test".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "12".to_string()
            }
        };
        let f2 = Filter {
            property: "test2".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::LessThan,
                value: "4".to_string()
            }
        };
        let f3 = Filter {
            property: "paid".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "true".to_string()
            }
        };
        let f4 = Filter {
            property: "value".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Different,
                value: "nothing".to_string()
            }
        };
        let f5 = Filter {
            property: "price".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::GreaterOrEqualThan,
                value: "400".to_string()
            }
        };
        // @test = 12 AND ( @test2 < 4 OR @paid = true ) AND ( @value != nothing OR @price >= 400 )
        let group1 = ExpressionGroup::FilterGroup(vec![FilterGroupComponent::Filter(f.clone())]);
        let group2 = ExpressionGroup::ParenthesisedGroup(
            vec![
                FilterGroupComponent::Filter(f2.clone()),
                FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
                FilterGroupComponent::Filter(f3.clone()),

            ]
        );
        let group3 = ExpressionGroup::ParenthesisedGroup(
            vec![
                FilterGroupComponent::Filter(f4.clone()),
                FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
                FilterGroupComponent::Filter(f5.clone()),

            ]
        );
        let expression1 = (group1.clone(), vec![
            (LogicalOperator::And, group2.clone()),
            (LogicalOperator::And, group3.clone())
        ]);
        let result1 = vec![
            ExpressionGroupComponent::ExpressionGroup(group1.clone()),
            ExpressionGroupComponent::LogicalOperator(LogicalOperator::And),
            ExpressionGroupComponent::ExpressionGroup(group2.clone()),
            ExpressionGroupComponent::LogicalOperator(LogicalOperator::And),
            ExpressionGroupComponent::ExpressionGroup(group3.clone()),
        ];
        assert_eq!(map_filter_expression_group_component(expression1), result1);
    }

    #[test]
    fn test_parse_multi_expression_group_component() {
        let f2 = Filter {
            property: "test2".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::LessThan,
                value: "4".to_string()
            }
        };
        let f3 = Filter {
            property: "paid".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "true".to_string()
            }
        };
        let f4 = Filter {
            property: "value".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Different,
                value: "nothing".to_string()
            }
        };
        let f5 = Filter {
            property: "price".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::GreaterOrEqualThan,
                value: "400".to_string()
            }
        };
        let query = "AND ( @test2 < 4 OR @paid = true ) AND ( @value != nothing OR @price >= 400 )";
        let group2 = ExpressionGroup::ParenthesisedGroup(
            vec![
                FilterGroupComponent::Filter(f2.clone()),
                FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
                FilterGroupComponent::Filter(f3.clone()),

            ]
        );
        let group3 = ExpressionGroup::ParenthesisedGroup(
            vec![
                FilterGroupComponent::Filter(f4.clone()),
                FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
                FilterGroupComponent::Filter(f5.clone()),

            ]
        );

        let expression1 = vec![
            (LogicalOperator::And, group2.clone()),
            (LogicalOperator::And, group3.clone())
        ];
        assert_eq!(parse_multi_expression_group_component(query), Ok(("", expression1)));
    }

    #[test]
    fn test_parse_expression() {
        let f = Filter {
            property: "test".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "12".to_string()
            }
        };
        let f2 = Filter {
            property: "test2".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::LessThan,
                value: "4".to_string()
            }
        };
        let f3 = Filter {
            property: "paid".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Equal,
                value: "true".to_string()
            }
        };
        let f4 = Filter {
            property: "value".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::Different,
                value: "nothing".to_string()
            }
        };
        let f5 = Filter {
            property: "price".to_string(),
            second_member: FilterSecondMember::Operator {
                operator: Operator::GreaterOrEqualThan,
                value: "400".to_string()
            }
        };
        // @test = 12 AND ( @test2 < 4 OR @paid = true ) AND ( @value != nothing OR @price >= 400 )
        let group1 = ExpressionGroup::FilterGroup(vec![FilterGroupComponent::Filter(f.clone())]);
        let group2 = ExpressionGroup::ParenthesisedGroup(
            vec![
                FilterGroupComponent::Filter(f2.clone()),
                FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
                FilterGroupComponent::Filter(f3.clone()),

            ]
        );
        let group3 = ExpressionGroup::ParenthesisedGroup(
            vec![
                FilterGroupComponent::Filter(f4.clone()),
                FilterGroupComponent::LogicalOperator(LogicalOperator::Or),
                FilterGroupComponent::Filter(f5.clone()),

            ]
        );
        let result1 = vec![
            ExpressionGroupComponent::ExpressionGroup(group1.clone()),
            ExpressionGroupComponent::LogicalOperator(LogicalOperator::And),
            ExpressionGroupComponent::ExpressionGroup(group2.clone()),
            ExpressionGroupComponent::LogicalOperator(LogicalOperator::And),
            ExpressionGroupComponent::ExpressionGroup(group3.clone()),
        ];
        let query = "@test = 12 AND ( @test2 < 4 OR @paid = true ) AND ( @value != nothing OR @price >= 400 )";
        assert_eq!(parse_expression(query), Ok(("", result1)));

        let query = "AND ( @test2 < 4 OR @paid = true ) AND ( @value != nothing OR @price >= 400 )";
        assert_eq!(parse_expression(query), Err(Error((query, Tag))));

    }
}