#![feature(test)]
extern crate test;

use cql_parser::parsers::expression;
use test::Bencher;

#[bench]
fn bench_parse_expression(b: &mut Bencher) {
    let query = "@test = 12 AND ( @test2 < 4 OR @paid = true ) AND ( @value != nothing OR ( @price >= 400 AND @data = 45 ) )";
    b.iter(||{
        expression::parse_expression(query)
    })
}